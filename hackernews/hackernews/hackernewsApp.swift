//
//  hackernewsApp.swift
//  hackernews
//
//  Created by Romeo Flauta on 10/22/20.
//

import SwiftUI

@main
struct hackernewsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
